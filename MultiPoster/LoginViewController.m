//
//  LoginViewController.m
//  MultiPoster
//
//  Created by Admin on 17.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "LoginViewController.h"

#import "LoginView.h"

#import "PushAnimator.h"
#import "PopAnimator.h"

#import "FacebookLoginViewController.h"
#import "VKLoginViewController.h"

#import "MessagesViewController.h"

#import "FacebookService.h"
#import "VKService.h"

@interface LoginViewController () <UINavigationControllerDelegate, VKLoginViewControllerDelegate, FacebookLoginViewControllerDelegate>

@property (nonatomic, strong) LoginView *loginView;

@property (nonatomic, strong) FacebookLoginViewController *facebookLoginController;
@property (nonatomic, strong) VKLoginViewController *VKLoginController;

@property (nonatomic, strong) PushAnimator  *pushAnimator;
@property (nonatomic, strong) PopAnimator *popAnimator;

@end

@implementation LoginViewController

- (void)loadView {
    self.loginView = [[LoginView alloc] init];
    self.view = self.loginView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pushAnimator = [[PushAnimator alloc] init];
    self.popAnimator = [[PopAnimator alloc] init];
    
    self.navigationController.delegate = self;
    [self.loginView.facebookLoginButton addTarget:self action:@selector(loginToFacebook:) forControlEvents:UIControlEventTouchUpInside];
    [self.loginView.vkLoginButton addTarget:self action:@selector(loginToVK:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    if (([[FacebookService sharedService] isLoggedIn])||([[VKService sharedService] isLoggedIn])){
            
            MessagesViewController *vc = [[MessagesViewController alloc] init];
            [self.navigationController pushViewController:vc animated:NO];
        }
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)loginToFacebook:(id)sender {
    self.facebookLoginController= [[FacebookLoginViewController alloc] init];
    self.facebookLoginController.delegate = self;
    [self.facebookLoginController loadWebConttent];
}

- (void)loginToVK:(id)sender {
    self.VKLoginController= [[VKLoginViewController alloc] init];
    self.VKLoginController.delegate = self;
    [self.VKLoginController loadWebContent];
}


- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController *)fromVC
                                                 toViewController:(UIViewController *)toVC {
    if (operation == UINavigationControllerOperationPop) {
        return _popAnimator;
    }
    else {
        return _pushAnimator;
    }
}

- (void)didLoadWebContent {
    [self.navigationController pushViewController:self.facebookLoginController animated:YES];
}
 
- (void)didLoadWebContentVK {
    [self.navigationController pushViewController:self.VKLoginController animated:YES];
}

- (void)didRecieveToken:(NSString *)token {
    [self.navigationController popViewControllerAnimated:YES];
    
    [[FacebookService sharedService] loginWithToken:token];
    
    MessagesViewController *vc = [[MessagesViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didRecieveTokenVK:(NSString *)token andUserId:(NSString*)UserId {
    [self.navigationController popViewControllerAnimated:YES];
    
    [[VKService sharedService] loginWithToken:token andUserId:UserId];
    
    MessagesViewController *vc = [[MessagesViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
