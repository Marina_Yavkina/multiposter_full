//
//  LoginView.m
//  MultiPoster
//
//  Created by Admin on 17.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "LoginView.h"
#import <QuartzCore/QuartzCore.h>

@implementation LoginView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"ImagePaperPlane"]];
        
        UIColor *textColor = [UIColor colorWithRed:16.0f / 255.0f
                                             green:94.0f / 255.0f
                                              blue:40.0f / 255.0f
                                             alpha:1.0f];
        
        UIColor *shadowColor = [UIColor colorWithRed:0.0f / 255.0f
                                               green:64.0f / 255.0f
                                                blue:19.0f / 255.0f
                                               alpha:1.0f];
        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"MultiPoster";
        _titleLabel.font = [UIFont systemFontOfSize:64.0f];
        _titleLabel.textColor = textColor;
        _titleLabel.shadowColor = [shadowColor colorWithAlphaComponent:0.4f];
        _titleLabel.shadowOffset = CGSizeMake(1.0f, 1.0f);
        
        [self addSubview:_titleLabel];
        
        _facebookLoginButton = [UIButton buttonWithType:UIButtonTypeSystem];
        
        [_facebookLoginButton setTitle:@"Log in with Facebook" forState:UIControlStateNormal];
        [_facebookLoginButton setTitleColor:textColor forState:UIControlStateNormal];
        _facebookLoginButton.contentEdgeInsets = UIEdgeInsetsMake(8.0f,
                                                                  8.0f,
                                                                  8.0f,
                                                                  8.0f);
        
        _facebookLoginButton.titleLabel.font = [UIFont systemFontOfSize:24.0f];
        _facebookLoginButton.layer.borderWidth = 2.0f;
        _facebookLoginButton.layer.borderColor = textColor.CGColor;
        _facebookLoginButton.layer.cornerRadius = 8.0f;
        
        [_facebookLoginButton.layer setShadowColor:shadowColor.CGColor];
        [_facebookLoginButton.layer setShadowOpacity:0.6f];
        [_facebookLoginButton.layer setShadowRadius:0.5];
        [_facebookLoginButton.layer setShadowOffset:CGSizeMake(1.0f, 1.0f)];
        
        [self addSubview:_facebookLoginButton];
        
        
        
        _vkLoginButton = [UIButton buttonWithType:UIButtonTypeSystem];
        
        [_vkLoginButton setTitle:@"Log in with VK" forState:UIControlStateNormal];
        [_vkLoginButton setTitleColor:textColor forState:UIControlStateNormal];
        _vkLoginButton.contentEdgeInsets = UIEdgeInsetsMake(8.0f,
                                                            8.0f,
                                                            8.0f,
                                                            8.0f);
        
        _vkLoginButton.titleLabel.font = [UIFont systemFontOfSize:24.0f];
        _vkLoginButton.layer.borderWidth = 2.0f;
        _vkLoginButton.layer.borderColor = textColor.CGColor;
        _vkLoginButton.layer.cornerRadius = 8.0f;
        
        [_vkLoginButton.layer setShadowColor:shadowColor.CGColor];
        [_vkLoginButton.layer setShadowOpacity:0.6f];
        [_vkLoginButton.layer setShadowRadius:0.5];
        [_vkLoginButton.layer setShadowOffset:CGSizeMake(1.0f, 1.0f)];
        
        [self addSubview:_vkLoginButton];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGSize facebookLoginButtonSize = [self.facebookLoginButton sizeThatFits:self.bounds.size];
    self.facebookLoginButton.frame = CGRectMake((self.bounds.size.width - facebookLoginButtonSize.width) / 2.0f,
                                                self.bounds.size.height / 2.0f + (self.bounds.size.height / 2.0f - facebookLoginButtonSize.height) / 2.0f,
                                                facebookLoginButtonSize.width,
                                                facebookLoginButtonSize.height);
    
    CGSize vkLoginButtonSize = facebookLoginButtonSize; //for equal size of buttons
    self.vkLoginButton.frame = CGRectMake((self.bounds.size.width - vkLoginButtonSize.width) / 2.0f,
                                          self.bounds.size.height / 2.0f + (self.bounds.size.height / 2.0f - vkLoginButtonSize.height) / 2.0f - 1.5f * vkLoginButtonSize.height,
                                          vkLoginButtonSize.width,
                                          vkLoginButtonSize.height);
    
    CGSize titleSize = [self.titleLabel sizeThatFits:self.bounds.size];
    self.titleLabel.frame = CGRectMake((self.bounds.size.width - titleSize.width) / 2.0f,
                                       (self.bounds.size.height / 2.0f - titleSize.height) / 2.0f,
                                       titleSize.width,
                                       titleSize.height);
}



@end
