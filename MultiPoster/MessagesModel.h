//
//  MessagesModel.h
//  MultiPoster
//
//  Created by Admin on 17.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Message;

@protocol MessagesModelDelegate <NSObject>

- (void)modelDidUpdate;

@end


@interface MessagesModel : NSObject
@property (nonatomic, weak) id<MessagesModelDelegate> delegate;

- (void)reloadDataWithCompletion:(void (^)())completion;

- (BOOL)canLoadNextPart: (NSString *) sourceType;
- (void)loadNextPartWithCompletion:(void (^)())completion;

- (NSInteger)numberOfRows;

- (Message *)itemForRow:(NSInteger)row;
@end
