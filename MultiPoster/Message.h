//
//  Message.h
//  MultiPoster
//
//  Created by Admin on 17.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "MappingObject.h"


@interface Message  : NSManagedObject<MappingObject> // <MappingObject>

@end

#import "Message+CoreDataProperties.h"
