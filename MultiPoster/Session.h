//
//  Session.h
//  MultiPoster
//
//  Created by Admin on 17.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Session : NSObject

@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *tokenVK;
@property (nonatomic, strong) NSString *UserId;
@property (nonatomic, strong) NSUserDefaults *userDefaults;

@end
