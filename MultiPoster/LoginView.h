//
//  LoginView.h
//  MultiPoster
//
//  Created by Admin on 17.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginView : UIView

@property (nonatomic, strong) UIImageView *backgroundImageView;

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *facebookLoginButton;
@property (nonatomic, strong) UIButton *vkLoginButton;

@end
