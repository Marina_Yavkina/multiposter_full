//
//  Service.h
//  MultiPoster
//
//  Created by Denis Fedorets on 20.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *FacebookServiceReachibilityChangedNotification;

typedef void (^FacebookServiceCompletion) (BOOL success, NSError *error);
typedef void (^FacebookServiceDataCompletion) (BOOL success, NSData *data, NSError *error);

@interface FacebookService : NSObject

+ (instancetype)sharedService;

- (BOOL)online;

- (void)loginWithToken:(NSString *)token;

- (BOOL)isLoggedIn;

- (void)logoutWithCompletion:(FacebookServiceCompletion)completion;

- (void)loadMediaWithCompletion:(FacebookServiceDataCompletion)completion;

- (NSString *)loadDataWithURL:(NSString *)urlString completion:(FacebookServiceDataCompletion)completion;

- (void)cancelTaskWithUUID:(NSString *)UUID;
- (void) sendUrlwithparams:(NSData*)image andText:(NSString*)text;
- (void) logOff;


@end
