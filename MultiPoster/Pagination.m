//
//  Pagination.m
//  MultiPoster
//
//  Created by Admin on 17.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "Pagination.h"

@implementation Pagination
+ (NSDictionary *)mapping {
    return @{
             @"next" : @"nextURL",
             @"next_max_id" : @"nextMaxId",
             };
}

+ (NSDictionary *)mappingVK {
    return @{
             @"offset" : @"nextURL",
             @"count" : @"nextMaxId",
             };
}
- (void)setNextMaxId:(NSString *)nextMaxId {
    NSString *str1 = [NSString stringWithFormat:@"%@",nextMaxId];
    if ([str1 containsString:@"_"])
    {
    NSArray *idParts = [nextMaxId componentsSeparatedByString:@"_"];
    NSAssert(idParts.count == 2, @"Wrong id pattern");
    
    self.nextPhotoId = @([idParts[0] integerValue]);
    }
    _nextMaxId = nextMaxId;
}

@end
