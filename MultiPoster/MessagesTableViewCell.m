//
//  MessagesTableViewCell.m
//  MultiPoster
//
//  Created by Admin on 17.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//
//
#import "MessagesTableViewCell.h"
#import "Message.h"

#import "FacebookService.h"
#import "VKService.h"

static CGFloat const MessagesTableViewCellContainerMargin = 8.0f;
static CGFloat const MessagesTableViewCellContentMargin = 8.0f;
static CGFloat const MessagesTableViewCellContentOffset = 8.0f;

@interface MessagesTableViewCell ()

@property (nonatomic, strong) UIView *containerView;

@property (nonatomic, strong) UIImageView *photoImageView;

@property (nonatomic, strong) UIImageView *logoImageView;

@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) UILabel *likesLabel;
@property (nonatomic, strong) UILabel *dtLabel;

@property (nonatomic, assign) UIEdgeInsets containerInsets;
@property (nonatomic, assign) UIEdgeInsets contentInsets;

@property (nonatomic, strong) NSString *imageDownloadTaskUUID;

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@end

@implementation MessagesTableViewCell

+ (NSString *)reuseIdentifier {
    return NSStringFromClass(self);
}



+ (CGFloat)heightForItem:(Message *)item widht:(CGFloat)widht {
    CGFloat actualWidht = widht - (MessagesTableViewCellContainerMargin * 2.0f + MessagesTableViewCellContentMargin * 2.0f);
    
    CGFloat height = MessagesTableViewCellContainerMargin * 2.0f + MessagesTableViewCellContentMargin * 2.0f;
    
    //if (item.url) {
    //    height += actualWidht;
    //}
    
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    NSDictionary *attributes = @{ NSFontAttributeName : [UIFont systemFontOfSize:[UIFont systemFontSize]],
                                  NSParagraphStyleAttributeName : paragraphStyle};
    
    NSString *likesString = [NSString stringWithFormat:@"%@ \uE022", item.likesCount];
    
    CGSize likesSize = [likesString boundingRectWithSize:CGSizeMake(actualWidht, CGFLOAT_MAX)
                                                 options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                              attributes:attributes
                                                 context:nil].size;
    
    if (item.photo_url) {
        height += actualWidht;
    }
    else{
        height += (3.0f*likesSize.height);
    }
    
    CGSize detailSize = [item.text boundingRectWithSize:CGSizeMake(actualWidht - MessagesTableViewCellContentOffset - likesSize.width, CGFLOAT_MAX)
                                                  options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                               attributes:attributes
                                                  context:nil].size;
    height += MAX(likesSize.height, detailSize.height) + MessagesTableViewCellContentOffset;
    return height;
}

+ (CGFloat)heightForItem:(Message *)item widht:(CGFloat)widht withImage:(UIImage *)image{
    CGFloat actualWidht = widht - (MessagesTableViewCellContainerMargin * 2.0f + MessagesTableViewCellContentMargin * 2.0f);
    
    CGFloat height = MessagesTableViewCellContainerMargin * 2.0f + MessagesTableViewCellContentMargin * 2.0f;
    
    if(image){
        CGFloat imageWidth = image.size.width;
        CGFloat imageHeight = image.size.height;
        CGFloat aspectForImage = imageWidth/imageHeight;
        height += actualWidht/aspectForImage;
    }
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    NSDictionary *attributes = @{ NSFontAttributeName : [UIFont systemFontOfSize:[UIFont systemFontSize]],
                                  NSParagraphStyleAttributeName : paragraphStyle};
    
    NSString *likesString = [NSString stringWithFormat:@"%@ \uE022", @"777"];
    CGSize likesSize = [likesString boundingRectWithSize:CGSizeMake(actualWidht, CGFLOAT_MAX)
                                                 options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                              attributes:attributes
                                                 context:nil].size;
    
    CGSize detailSize = [item.text boundingRectWithSize:CGSizeMake(actualWidht - MessagesTableViewCellContentOffset - likesSize.width, CGFLOAT_MAX)
                                                  options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                               attributes:attributes
                                                  context:nil].size;
    height += MAX(likesSize.height, detailSize.height) + MessagesTableViewCellContentOffset;
    return height;
}

- (instancetype)init {
    return [self initWithReuseIdentifier:[MessagesTableViewCell reuseIdentifier]];
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    return [self initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _containerInsets = UIEdgeInsetsMake(MessagesTableViewCellContainerMargin,
                                            MessagesTableViewCellContainerMargin,
                                            MessagesTableViewCellContainerMargin,
                                            MessagesTableViewCellContainerMargin);
        
        _contentInsets = UIEdgeInsetsMake(MessagesTableViewCellContentMargin,
                                          MessagesTableViewCellContentMargin,
                                          MessagesTableViewCellContentMargin,
                                          MessagesTableViewCellContentMargin);
        
        _containerView = [[UIView alloc] init];
        _containerView.layer.cornerRadius = 5.0f;
        _containerView.backgroundColor = [UIColor colorWithRed:148.0f / 255.0f
                                                         green:194.0f / 255.0f
                                                          blue:207.0f / 255.0f
                                                         alpha:1.0f];
        [self.contentView addSubview:_containerView];
        
        _photoImageView = [[UIImageView alloc] init];
        //_photoImageView.backgroundColor = [UIColor lightGrayColor];
        _photoImageView.backgroundColor = [UIColor clearColor];
        _photoImageView.contentMode = UIViewContentModeScaleAspectFit;
        [_containerView addSubview:_photoImageView];
        
        _logoImageView = [[UIImageView alloc] init];
        _logoImageView.backgroundColor = [UIColor clearColor];
        _logoImageView.contentMode = UIViewContentModeScaleAspectFit;
        [_containerView addSubview:_logoImageView];
        
        _descriptionLabel = [[UILabel alloc] init];
        _descriptionLabel.numberOfLines = 0;
        _descriptionLabel.font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
        [_containerView addSubview:_descriptionLabel];
        
        _likesLabel = [[UILabel alloc] init];
        _likesLabel.font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
        [_containerView addSubview:_likesLabel];
        
        _dtLabel = [[UILabel alloc] init];
        _dtLabel.font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
        [_containerView addSubview:_dtLabel];
        
        _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _indicatorView.hidden = YES;
        [self.photoImageView addSubview:_indicatorView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.containerView.frame = UIEdgeInsetsInsetRect(self.bounds, self.containerInsets);
    
    CGRect actualBounds = UIEdgeInsetsInsetRect(self.containerView.bounds, self.contentInsets);
    
    CGSize likesSize = [self.likesLabel sizeThatFits:CGSizeMake(actualBounds.size.width, CGFLOAT_MAX)];
    CGSize descriptionSize = [self.descriptionLabel sizeThatFits:CGSizeMake(actualBounds.size.width - likesSize.width - MessagesTableViewCellContentOffset, CGFLOAT_MAX)];
    
    CGSize dtSize = [self.dtLabel sizeThatFits:CGSizeMake(actualBounds.size.width, CGFLOAT_MAX)];
    
    self.dtLabel.frame = CGRectMake(actualBounds.origin.x + (actualBounds.size.width-dtSize.width)/2.0f,
                                          actualBounds.origin.y,
                                          dtSize.width,
                                          dtSize.height);
    
    self.logoImageView.frame = CGRectMake(actualBounds.origin.x,
                                          actualBounds.origin.y,
                                          3*likesSize.height,
                                          3*likesSize.height);
    
    self.likesLabel.frame = CGRectMake(actualBounds.origin.x + (actualBounds.size.width - likesSize.width),
                                       actualBounds.origin.y + MessagesTableViewCellContentOffset + self.logoImageView.frame.size.height/2.0f-self.likesLabel.frame.size.height/2.0f,
                                       likesSize.width,
                                       likesSize.height);
    
    self.descriptionLabel.frame = CGRectMake(actualBounds.origin.x,
                                             CGRectGetMaxY(self.logoImageView.frame) + MessagesTableViewCellContentOffset,
                                             descriptionSize.width,
                                             descriptionSize.height);
    
    self.photoImageView.frame = CGRectMake(actualBounds.origin.x,
                                           CGRectGetMaxY(self.descriptionLabel.frame)+(descriptionSize.height>0?MessagesTableViewCellContentOffset:0),
                                           actualBounds.size.width,
                                           actualBounds.size.width-(descriptionSize.height>0?4.0f:1.0f)*MessagesTableViewCellContentOffset-self.logoImageView.frame.size.height+self.likesLabel.frame.size.height+MessagesTableViewCellContentOffset);
    
    self.indicatorView.center = CGPointMake(self.photoImageView.bounds.size.width / 2.0f,
                                            self.photoImageView.bounds.size.height / 2.0f);}

- (void)setItem:(Message *)item {
    
    self.descriptionLabel.text = item.text;
    self.likesLabel.text = [NSString stringWithFormat:@"%@ \uE022", item.likesCount];
   
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MMMM dd, yyyy HH:mm:ss"];
    self.dtLabel.text = [format stringFromDate:item.message_dt];

    self.indicatorView.hidden = NO;
    [self.indicatorView startAnimating];
    
    //TO DO: здесь нужно устроить проверку на то, из какой соцсети пришли данные
    if ([item.source_type isEqualToString:@"facebook"]) {
        self.logoImageView.image = [UIImage imageNamed:@"facebookLogo"];
    }
    else if ([item.source_type isEqualToString:@"vk"]){
        self.logoImageView.image = [UIImage imageNamed:@"vkLogo"];
    }
    
    if (item.photo_url) {
        
        [self.photoImageView setHidden:NO];
        
        if ([item.source_type isEqualToString:@"facebook"]) {
            self.imageDownloadTaskUUID = [[FacebookService sharedService] loadDataWithURL:item.photo_url
                                                                               completion:^(BOOL success, NSData *data, NSError *error) {
                                                                                   UIImage *image = [UIImage imageWithData:data];
                                                                                   self.photoImageView.image = image;
                                                                                   
                                                                                   self.indicatorView.hidden = YES;
                                                                                   [self.indicatorView stopAnimating];
                                                                               }];
        }
        else if ([item.source_type isEqualToString:@"vk"]){
            self.imageDownloadTaskUUID = [[VKService sharedService] loadDataWithURL:item.photo_url
                                                                               completion:^(BOOL success, NSData *data, NSError *error) {
                                                                                   UIImage *image = [UIImage imageWithData:data];
                                                                                   self.photoImageView.image = image;
                                                                                   
                                                                                   self.indicatorView.hidden = YES;
                                                                                   [self.indicatorView stopAnimating];
                                                                               }];
        }
        

    }
    else{
        [self.photoImageView setHidden:YES];
    }
    
    
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.photoImageView.image = nil;
    
    self.indicatorView.hidden = YES;
    [self.indicatorView stopAnimating];
    
    if (self.imageDownloadTaskUUID) {
        [[FacebookService sharedService] cancelTaskWithUUID:self.imageDownloadTaskUUID];
        [[VKService sharedService] cancelTaskWithUUID:self.imageDownloadTaskUUID];
        self.imageDownloadTaskUUID = nil;
    }
}

@end
